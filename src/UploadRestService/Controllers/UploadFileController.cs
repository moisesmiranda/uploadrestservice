﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using UploadRestService.Models;

namespace UploadRestService.Controllers
{
    public class UploadFileController : ApiController
    {
        static readonly string ServerUploadFolder = @"C:\SIPEF_UPLOADED";

        [HttpPost]
        public async Task<FileInformation> UploadFile()
        {
            if (!Directory.Exists(ServerUploadFolder))
                Directory.CreateDirectory(ServerUploadFolder);
            // Verify that this is an HTML Form file upload request
            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.UnsupportedMediaType));
            }

            // Create a stream provider for setting up output streams
            MultipartFormDataStreamProvider streamProvider = new MultipartFormDataStreamProvider(ServerUploadFolder);

            // Read the MIME multipart asynchronously content using the stream provider we just created.
            await Request.Content.ReadAsMultipartAsync(streamProvider);

            var fileName = streamProvider.FileData.LastOrDefault().LocalFileName;
            

            // Create response
            return new FileInformation
            {
                FileNames = streamProvider.FileData.Select(entry => entry.LocalFileName),
                Submitter = streamProvider.FormData["submitter"]
            };
        }
    }
}
