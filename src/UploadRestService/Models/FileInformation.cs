﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UploadRestService.Models
{
    public class FileInformation
    {
        public IEnumerable<string> FileNames { get; set; }

        public string Submitter { get; set; }
    }
}